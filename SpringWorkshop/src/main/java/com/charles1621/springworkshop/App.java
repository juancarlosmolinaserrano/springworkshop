package com.charles1621.springworkshop;

import com.charles1621.springworkshop.domain.User;
import com.charles1621.springworkshop.service.DateService;
import com.charles1621.springworkshop.service.GreetingService;
import com.charles1621.springworkshop.service.UserService;
import com.charles1621.springworkshop.service.impl.DateServiceImpl;
import com.charles1621.springworkshop.service.impl.GreetingServiceImpl;
import com.charles1621.springworkshop.service.impl.UserServiceImpl;

/**
 *
 * @author charl
 */
public class App {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        UserService userService = new UserServiceImpl("jdbc:mysql://localhost/viaja_seguro", "root", "12345");
        DateService dateService = new DateServiceImpl();
		GreetingService greetingService = new GreetingServiceImpl(dateService);
        greetingService.setGreeting("Buenos dias");
        greetingService.setIncludeDate(true);
        
        User user = userService.findOneById(1);
		greetingService.setUser(user);
		System.out.println(greetingService.greet());
    }
    
}
