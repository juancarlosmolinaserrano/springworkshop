package com.charles1621.springworkshop.service;

import com.charles1621.springworkshop.domain.User;

/**
 *
 * @author charl
 */
public interface GreetingService {
    
    String greet();
    
    String getGreeting();

    void setGreeting(String greeting);

    boolean isIncludeDate();

    void setIncludeDate(boolean includeDate);

    User getUser();

    void setUser(User user);

    DateService getDateService();

    void setDateService(DateService dateService);
    
}
