package com.charles1621.springworkshop.service.impl;

import com.charles1621.springworkshop.service.DateService;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author charl
 */
public class DateServiceImpl implements DateService {
    
    public String formattedToday() {
        return new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(new Date());
    }
    
}
