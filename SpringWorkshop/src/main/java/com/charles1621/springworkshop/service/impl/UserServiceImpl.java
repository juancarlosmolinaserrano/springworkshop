package com.charles1621.springworkshop.service.impl;

import com.charles1621.springworkshop.domain.User;
import com.charles1621.springworkshop.service.UserService;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author charl
 */
public class UserServiceImpl implements UserService {
    
    private String url;
    private String username;
    private String password;
    
    private List<User> users = new ArrayList();
    
    public UserServiceImpl(String url, String username, String password) {
        this.url = url;
        this.username = username;
        this.password = password;
        
        users = new ArrayList();
        users.add(new User(1, "charles1621@hotmail.com", "Juan Carlos"));
        users.add(new User(2, "jc4testing@gmail.com", "Carlos"));
    }

    public List<User> findAll() {
        return users;
    }
    
    public User findOneById(int id) {
        for(User user : users) {
            if(user.getId() == id) {
                return user;
            }
        }
        return null;
    }
    
}
