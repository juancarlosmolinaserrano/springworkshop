package com.charles1621.springworkshop.service.impl;

import com.charles1621.springworkshop.domain.User;
import com.charles1621.springworkshop.service.DateService;
import com.charles1621.springworkshop.service.GreetingService;

/**
 *
 * @author charl
 */
public class GreetingServiceImpl implements GreetingService {

    private String greeting = "Hola";
	private boolean includeDate = true;
    private User user;
	private DateService dateService;
	
	public GreetingServiceImpl(DateService dateService) {
		this.dateService = dateService;
	}

    @Override
	public String greet() {
		String greeting = getGreeting();
        if (user != null) {
            greeting += " " + user.getName();
        }
		if (includeDate) {
			greeting += ", hoy es: " + dateService.formattedToday();
		}
		return greeting;
	}

    public String getGreeting() {
        return greeting;
    }

    public void setGreeting(String greeting) {
        this.greeting = greeting;
    }

    public boolean isIncludeDate() {
        return includeDate;
    }

    public void setIncludeDate(boolean includeDate) {
        this.includeDate = includeDate;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public DateService getDateService() {
        return dateService;
    }

    public void setDateService(DateService dateService) {
        this.dateService = dateService;
    }
    
}
