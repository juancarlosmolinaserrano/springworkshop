package com.charles1621.springworkshop.service;

import com.charles1621.springworkshop.domain.User;
import java.util.List;

/**
 *
 * @author charl
 */
public interface UserService {

    List<User> findAll();
    
    User findOneById(int id);
    
}
